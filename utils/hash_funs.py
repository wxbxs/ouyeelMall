"""
哈希字符串
2021-2-22
"""
import hashlib
import json


def my_hash(string, hash_type='md5'):
    """
    哈希一个字符串，可以选择md5或者sha1
    :param string:
    :param hash_type:
    :return:
    """
    if not isinstance(string, str):
        string = json.dumps(string)

    if hash_type == 'md5':
        # 32位
        md5 = hashlib.md5()
    else:
        # 40位
        md5 = hashlib.sha1()

    md5.update(string.encode('utf-8'))
    return md5.hexdigest()


def hash_int(string):
    """
    哈希一个字符串为int
    :param string:
    :return:
    """
    md5_value = my_hash(string)
    int_hash = hash(int(md5_value, 16))
    return int_hash


if __name__ == '__main__':
    print(hash_int('123'))
    print(len(my_hash('123', hash_type='sha1')))

