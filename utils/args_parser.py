# -*- coding: utf-8 -*-
# CreateDate : 2021/12/15 23:24
# Author     : 不肖生
# Github     :
# EditDate   :
# sourceProj : wuliu_news
# Description:

import argparse


def parse_cmd_params(sys_args):
    """
    :param sys_args:
    :return:
    """
    Parser = argparse.ArgumentParser(add_help=True)
    Parser.add_argument('-n', '--name', dest='spider', metavar='spider name', help=f"spider name", required=True)
    Parser.add_argument('-p', '--params', dest='params', metavar='params',
                        help="附加参数: key1=value1 key2=value2", required=False, nargs='*')

    Params = Parser.parse_args(sys_args)

    setattr(Params, 'params', parse_more_params(Params.params))

    return Params


def parse_more_params(params: str):
    """
    将 ['key1=value1', 'key2=value2'] 转换为字典 {'key1': 'value1', 'key2': 'value2'}
    :param params:
    :return:
    """
    if not params:
        return {}
    param_dict = {p.split('=')[0]: p.split('=')[1] for p in params if '=' in p}
    return param_dict
