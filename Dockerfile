FROM python:3.7
ENV PATH /usr/local/bin:$PATH
ADD . /ouyeelMall
WORKDIR /ouyeelMall
RUN pip3.7 install -r requirements.txt -i https://pypi.douban.com/simple/
RUN playwright install
CMD playwright install && scrapy crawl ouyeel