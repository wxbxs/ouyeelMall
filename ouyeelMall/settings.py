# Scrapy settings for ouyeelMall project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html
from datetime import datetime

from ouyeelMall.constant import Default_User_Agent

BOT_NAME = 'ouyeelMall'

SPIDER_MODULES = ['ouyeelMall.spiders']
NEWSPIDER_MODULE = 'ouyeelMall.spiders'

# ! 以下根据需要调整
# 智汇供应链网站域名，可以根据需要更改为线上或者测试
# BASE_WEBSITE = "http://wuliu.akng.net"
BASE_WEBSITE = "http://isc.chinascm.org.cn"

# 根据用户定义的任务周期更改此配置，例如：如果任务每2天运行一次则设为 2，程序遇到2天之前的日期会停止爬取
CRAWL_CYCLE = 7

# 爬取间隔秒数，可以调节爬取速度，数值越小越快，数据量不大，建议不要太小
DOWNLOAD_DELAY = 5

# ! 以上根据需要调整


# 来源网站
SOURCE_DOMAIN = "https://www.ouyeel.com"

# 程序爬取信息的当前日期
TODAY = datetime.now().date()

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'wuliu_news (+http://www.yourdomain.com)'
USER_AGENT = Default_User_Agent

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
CONCURRENT_REQUESTS_PER_IP = 1

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
    'Accept': 'application/json,text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6',
}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'ouyeelMall.middlewares.OuyeelmallSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
   'ouyeelMall.middlewares.OuyeelmallDownloaderMiddleware': 543,
   'ouyeelMall.middlewares.SeleniumDownloaderMiddleware': 100,
}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'ouyeelMall.pipelines.OuyeelmallPipeline': 300,
   'ouyeelMall.pipelines.OuyeelmallDuplicatePipeline': 200,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
AUTOTHROTTLE_ENABLED = True
# The initial download delay
AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# HTTPCACHE_ENABLED = True
HTTPCACHE_EXPIRATION_SECS = 1800
# HTTPCACHE_DIR = 'httpcache'
HTTPCACHE_IGNORE_HTTP_CODES = [500, 502, 503, 504, 522, 524, 408, 429, 402, 407, 404, 403, 409, 302]
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

RANDOMIZE_DOWNLOAD_DELAY = True
# AUTOTHROTTLE_DEBUG = True
# AUTOTHROTTLE_START_DELAY = 0.1
RETRY_TIMES = 2
RETRY_HTTP_CODES = [500, 502, 503, 504, 522, 524, 400]
# COOKIES_DEBUG = True
REDIRECT_ENABLED = False
# 自动根据生成request的response添加referer
# REFERER_ENABLED = True
DOWNLOAD_TIMEOUT = 60
# 图片下载有效时间，未验证
# IMAGE_EXPIRE = 90
# REDIS_ITEMS_SERIALIZER = 'json.dumps'

# LOG_LEVEL = "DEBUG"
