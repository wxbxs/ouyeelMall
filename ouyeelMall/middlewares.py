# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
import random
import re
import time

# from playwright.sync_api import sync_playwright, Route
from scrapy import signals

# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter
from scrapy.http import HtmlResponse
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from ouyeelMall.api.seleniumcrawler import SeleniumBrowser


class OuyeelmallSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class OuyeelmallDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


# def cancel_request(route: Route, request):
#     route.abort(error_code=None)


class SeleniumDownloaderMiddleware(object):
    def __init__(self):
        pass

    def process_request(self, request, spider):
        """"""
        # by selenium
        # # if spider.name == "ouyeel":
        sb = SeleniumBrowser()
        browser = sb.browser
        browser.get("https://www.ouyeel.com/")

        # productType&channal&shopsign
        url = request.url
        time.sleep(random.randint(5, 7) - random.random())
        browser.get(url)
        time.sleep(random.randint(3, 5) + random.random())
        try:
            WebDriverWait(browser, 30).until(
                EC.presence_of_element_located(
                    (By.CSS_SELECTOR, 'div[class^="imageArea"]')
                )
            )
        except TimeoutException:
            pass
        time.sleep(random.randint(5, 10) + random.random())
        html = browser.page_source

        # by playwright webkit

        # url = request.url
        #
        # with sync_playwright() as p:
        #     browser = p.webkit.launch(headless=True)
        #     context = browser.new_context()
        #     page = context.new_page()
        #
        #     page.route(url=re.compile(r"(\.png)|(\.jpg)"), handler=cancel_request)
        #
        #     page.goto("https://www.ouyeel.com")
        #     page.wait_for_load_state()
        #     time.sleep(random.randint(3, 5) + random.random())
        #     page.goto(url)
        #     page.wait_for_load_state()  # state="networkidle"
        #     time.sleep(random.randint(3, 5) + random.random())
        #     html = page.content()

        return HtmlResponse(url=url, body=html, encoding="utf8", request=request)

