# -*- coding: utf-8 -*-
# CreateDate :		#2021/12/22 01:27
# Author     :		#不肖生
# Github     :		#https://gitee.com/wxbxs/ouyeelMall
# EditDate   :		#
# sourceProj :		#wuliu_news
# Description:
from datetime import datetime

from ouyeelMall.api.dicts.mall_dict import IronPayloadKeys
from ouyeelMall.baseitems.baseitem import BaseItem
from ouyeelMall.settings import BASE_WEBSITE


class MallProductItem(BaseItem):
    title: str  # 钢材商品测试
    amount: int  # 1
    unit: str  # 件  # 件/吨
    brand: str  # brand
    sourceurl: str  # beijing.com
    pname: str  # 北京
    content: str  # &nbsp;钢材商品详情
    addtime: str  # 2021-12-22 00:54:11

    n1: str  # attr1
    v1: str  # value1|value11
    n2: str  # attr2
    v2: str  # value2|value22
    n3: str  #
    v3: str  #

    thumb: str  # http://wuliu.akng.net/file/upload/202112/22/005523871.gif.thumb.gif
    thumb1: str  #
    thumb2: str  #

    moduleid: int  # 16
    file: str  # index
    action: str  # add
    itemid: int  # 0
    forward: str  # http://wuliu.akng.net/admin.php?moduleid=16
    mycatid: int  #
    catid: int  # 46855
    level: int  # 0
    style: str  #
    step_a1: int  # 1
    step_p1: float  # 1
    step_a2: str  #
    step_p2: float  #
    step_a3: str  #
    step_p3: float  #
    source: str  # ouyeel
    hyname: str  # 钢铁行业
    express_name_1: str  #
    fee_start_1: float  #
    fee_step_1: float  #
    express_1: int  # 0
    express_name_2: str  #
    fee_start_2: float  #
    fee_step_2: float  #
    express_2: int  # 0
    express_name_3: str  #
    fee_start_3: float  #
    fee_step_3: float  #
    express_3: int  # 0
    cod: int  # 0
    username: str  # admin
    elite: int  # 0
    status: int  # 3
    note: str  #
    hits: int  # 0
    fee: int  #
    template: str  #
    submit: str  # 确 定

    def __init__(self, cat_id, hyname="钢铁行业", source="ouyeel.com", moduleid=16, **kwargs):
        BaseItem.__init__(self, **kwargs)

        self.catid = cat_id
        self.moduleid = moduleid
        self.file = "index"
        self.action = "add"
        self.hyname = hyname
        self.source = source
        self.itemid = 0
        self.submit = "确 定"
        self.status = 3
        self.level = 0

        self.step_a1 = 1

        self.username = "admin"

        bw = BASE_WEBSITE.rstrip('/')
        self.forward = f"{bw}/admin.php?moduleid={self.moduleid}"

        self.addtime = str(datetime.now())[:19]

    @property
    def values(self):
        """
        转换 baseitems 与 payload 之间的 key
        :return:
        """
        values = dict()
        for k, v in self.__dict__.items():
            values[IronPayloadKeys.get(k, k)] = v
        return values
