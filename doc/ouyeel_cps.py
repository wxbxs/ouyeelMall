Querys = [
    {
        "code": "BXG",
        "name": "不锈钢",
        "productType": [
            {
                "name": "冷轧不锈钢板卷",
                "value": "BL11",
                "shop_signs": [
                    "B436D",
                    "B442D",
                    "SUS304",
                    "B430LNT",
                    "B443NT",
                    "304L",
                    "B441",
                    "439",
                    "1.4509",
                    "SUS430",
                    "2205",
                    "304"
                ]
            },
            {
                "name": "热轧不锈钢中厚板",
                "value": "BI22",
                "shop_signs": [
                    "1Cr20Ni14Si2",
                    "304",
                    "304L",
                    "316",
                    "316L",
                    "S32205"
                ]
            },
            {
                "name": "不锈钢圆管",
                "value": "BH21",
                "shop_signs": []
            },
            {
                "name": "不锈钢棒材",
                "value": "BB1A",
                "shop_signs": []
            },
            {
                "name": "热轧不锈钢卷",
                "value": "BI11",
                "shop_signs": []
            }
        ]
    },
    {
        "code": "GG",
        "name": "硅钢",
        "productType": [
            {
                "name": "无取向电工钢板卷",
                "value": "TL71",
                "shop_signs": [
                    "50WW800",
                    "B50A600",
                    "B27A230",
                    "B50A800",
                    "B50A1300",
                    "50WW1300",
                    "50WW600",
                    "B35A270",
                    "50AGW800"
                ]
            },
            {
                "name": "无取向电工钢尾卷",
                "value": "TL79",
                "shop_signs": [
                    "无取向钢让步小卷",
                    "无取向电工钢尾卷",
                    "无取向钢让步卷",
                    "B25AH230",
                    "B30AH230",
                    "B27A230",
                    "无取向电工钢卷",
                    "WSDX-50"
                ]
            },
            {
                "name": "无取向电工钢可利用材",
                "value": "TL78",
                "shop_signs": [
                    "35SW440(可利用..."
                ]
            },
            {
                "name": "热轧电工钢基料",
                "value": "TI1G",
                "shop_signs": [
                    "XG1300WR",
                    "XG1300WR-1",
                    "XG250WG",
                    "XG300WG",
                    "XG600WR",
                    "XG800WR"
                ]
            },
            {
                "name": "取向电工钢尾板卷",
                "value": "TL7C",
                "shop_signs": [
                    "取向硅钢尾板"
                ]
            },
            {
                "name": "取向电工钢板卷",
                "value": "TL72",
                "shop_signs": [
                    "KLYP",
                    "27GO无涂层塌卷料",
                    "30QG120"
                ]
            }
        ]
    },
    {
        "code": "GJ",
        "name": "钢筋",
        "productType": [
            {
                "name": "螺纹钢",
                "value": "TJ11",
                "shop_signs": [
                    "HRB400E",
                    "HRB400"
                ]
            },
            {
                "name": "盘螺",
                "value": "TJ13",
                "shop_signs": [
                    "HRB400E",
                    "HRB400"
                ]
            }
        ]
    },
    {
        "code": "GXC",
        "name": "钢管",
        "productType": [
            {
                "name": "焊接钢管",
                "value": "GHA3",
                "shop_signs": [
                    "Q235"
                ]
            },
            {
                "name": "热镀锌钢管",
                "value": "GHA2",
                "shop_signs": [
                    "Q235"
                ]
            },
            {
                "name": "热轧输送管",
                "value": "GW51",
                "shop_signs": [
                    "Q345E",
                    "20",
                    "45",
                    "A 106B",
                    "STPG 370|ST...",
                    "25Mn",
                    "Q345D"
                ]
            },
            {
                "name": "热轧结构管",
                "value": "GW52",
                "shop_signs": [
                    "SAE 1020",
                    "BG890QL",
                    "34CrMo4",
                    "Q390D",
                    "16MnCrMoVW",
                    "20",
                    "30CrMnSiA",
                    "42MnMo7",
                    "BG735LRS",
                    "C1026"
                ]
            },
            {
                "name": "低温用无缝钢管",
                "value": "GW53",
                "shop_signs": [
                    "A 333-6"
                ]
            },
            {
                "name": "037管",
                "value": "GW54",
                "shop_signs": [
                    "20"
                ]
            },
            {
                "name": "热轧管线管",
                "value": "GW61",
                "shop_signs": [
                    "B|A 53B|A 1...",
                    "B|A 53B|A 1...",
                    "X42N",
                    "BNS"
                ]
            },
            {
                "name": "热轧钻探管",
                "value": "GW62",
                "shop_signs": [
                    "DZ40",
                    "BG27CrMo",
                    "BG850ZT",
                    "BGXM105ZT"
                ]
            },
            {
                "name": "热轧低中压锅炉管",
                "value": "GW68",
                "shop_signs": [
                    "20"
                ]
            },
            {
                "name": "热轧高压锅炉管",
                "value": "GW21",
                "shop_signs": [
                    "SA-210C",
                    "SA-335P22",
                    "20G",
                    "20",
                    "A 213T22",
                    "A 335P22",
                    "SA-210A1",
                    "SA-213T12"
                ]
            },
            {
                "name": "螺旋焊管",
                "value": "GH23",
                "shop_signs": [
                    "L360M",
                    "L290M",
                    "L485M",
                    "Q235",
                    "Q355B"
                ]
            },
            {
                "name": "ERW焊接管线管",
                "value": "GH14",
                "shop_signs": []
            }
        ]
    },
    {
        "code": "LJ",
        "name": "冷系板卷",
        "productType": [
            {
                "name": "冷轧板卷",
                "value": "TL11",
                "shop_signs": [
                    "SPCC",
                    "JSC270C",
                    "St12",
                    "DC01",
                    "BLC",
                    "SYHS",
                    "SPCC-SD",
                    "DC01EK",
                    "SPCC-YT",
                    "SPCD",
                    "JSC270D",
                    "DC03"
                ]
            },
            {
                "name": "冷轧纵剪带钢",
                "value": "TL1E",
                "shop_signs": [
                    "50#",
                    "15CrMo",
                    "30CrMo",
                    "50CrV4",
                    "65Mn",
                    "SCM415",
                    "GCR15",
                    "SK5",
                    "20CR",
                    "15CRMO",
                    "50CRV4",
                    "T8A"
                ]
            },
            {
                "name": "冷轧带钢",
                "value": "TL1F",
                "shop_signs": [
                    "SPCC",
                    "DC01",
                    "DC04",
                    "HC220Y",
                    "09CuPCrNi-A",
                    "B250P1",
                    "B280VK",
                    "CR420LA",
                    "JSC590R",
                    "HC340/590DP"
                ]
            },
            {
                "name": "冷轧尾板卷",
                "value": "TL19",
                "shop_signs": [
                    "SPCC",
                    "DC01",
                    "SAE 1010",
                    "SAE1008",
                    "SPCC-YT",
                    "DC03",
                    "BLD",
                    "JSC270E",
                    "DC04",
                    "DC05",
                    "DC06",
                    "B210P1"
                ]
            },
            {
                "name": "冷轧盒板",
                "value": "TL1D",
                "shop_signs": [
                    "SPCC",
                    "St12",
                    "DC01",
                    "SPCC(DC01)",
                    "DC03",
                    "DC04",
                    "50#",
                    "45#",
                    "20#",
                    "ST12",
                    "Q235",
                    "08AL",
                    "10#"
                ]
            },
            {
                "name": "冷轧窄带",
                "value": "TL12",
                "shop_signs": [
                    "DC04",
                    "DC06",
                    "HC260Y",
                    "HC420LA",
                    "30CrMo",
                    "20CrMo",
                    "65Mn",
                    "50CrV",
                    "68CrNiMo",
                    "30CRMO",
                    "35CrMo"
                ]
            },
            {
                "name": "冷轧横切卷",
                "value": "TL14",
                "shop_signs": [
                    "20crmo"
                ]
            },
            {
                "name": "热镀锌板卷",
                "value": "TL21",
                "shop_signs": [
                    "DC51D+Z",
                    "DC51D+ZF",
                    "DX51D+Z",
                    "DX51D+ZF",
                    "SGCC",
                    "JAC270C",
                    "DX51D+Z-MD",
                    "DC51D+Z-MD",
                    "DX51D+Z80"
                ]
            },
            {
                "name": "锌铁合金板卷",
                "value": "TL27",
                "shop_signs": [
                    "DC51D+ZF",
                    "DX51D+Z",
                    "DX51D+ZF",
                    "SGCD1",
                    "JAC270D",
                    "SCGA270D",
                    "SCGA270DX",
                    "MJAC270D",
                    "DC53D+ZF"
                ]
            },
            {
                "name": "热镀锌尾板卷",
                "value": "TL29",
                "shop_signs": [
                    "DC51D+Z",
                    "DC53D+ZF",
                    "HR550LA",
                    "JAC440W",
                    "HC340LAD+Z",
                    "HC420/780DP...",
                    "HC420/780DP...",
                    "热镀锌尾卷"
                ]
            },
            {
                "name": "热轧基板热镀锌",
                "value": "TL26",
                "shop_signs": [
                    "DX51D+Z",
                    "SGCC",
                    "STM3",
                    "S550GD+ZM",
                    "S350GD+ZM",
                    "DX51D+ZM-T",
                    "SGH440D+ZM",
                    "STG3",
                    "SGH340D+ZM"
                ]
            },
            {
                "name": "无锌花镀锌板卷",
                "value": "TL2B",
                "shop_signs": [
                    "CR1",
                    "DX51D+Z",
                    "SGCC",
                    "DX51D+Z80",
                    "DX52D+Z",
                    "DC56D+Z",
                    "DX57D+Z",
                    "DX56D+Z",
                    "SGH340",
                    "DC51D+Z80"
                ]
            },
            {
                "name": "纯锌热镀锌板卷",
                "value": "TL2C",
                "shop_signs": [
                    "DX51D+Z",
                    "DC53D+Z",
                    "DX53D+Z",
                    "DC53D+Z(DC5...",
                    "DC54D+Z",
                    "DX54D+Z",
                    "CR4",
                    "DC53D+Z(DC5...",
                    "DC56D+Z"
                ]
            },
            {
                "name": "热镀锌耐指纹板卷",
                "value": "TL25",
                "shop_signs": [
                    "DX51D+Z",
                    "SGCC",
                    "DC51D+Z 40/...",
                    "DC51D+Z 40/...",
                    "DX53D-NF",
                    "DX53D+Z",
                    "DC51D+Z-N5",
                    "DX51D+Z80AF"
                ]
            },
            {
                "name": "热镀锌窄带",
                "value": "TL23",
                "shop_signs": [
                    "DC51D+Z",
                    "DX51D+Z",
                    "HC180BD+Z",
                    "H220BD+Z",
                    "HC220BD+Z",
                    "HC380/590TR...",
                    "S550GD+ZM",
                    "S450GD+ZM"
                ]
            },
            {
                "name": "有锌花镀锌板卷",
                "value": "TL22",
                "shop_signs": [
                    "DC51D+Z",
                    "SGCC",
                    "DX51D+Z80",
                    "HC220BD+ZF",
                    "JAC590Y",
                    "SGCC-R",
                    "SGH400",
                    "BGH340-275",
                    "SGH440"
                ]
            },
            {
                "name": "锌铁合金尾板卷",
                "value": "TL28",
                "shop_signs": [
                    "SCGA270D",
                    "DX52D+ZF"
                ]
            },
            {
                "name": "镀锌镁合金",
                "value": "TL2H",
                "shop_signs": [
                    "DC51D+ZM",
                    "DX51D+ZM-T",
                    "DX54D+ZM"
                ]
            },
            {
                "name": "镀锌带钢",
                "value": "TL2D",
                "shop_signs": []
            },
            {
                "name": "彩涂板卷",
                "value": "TL41",
                "shop_signs": [
                    "TDC51D+AZ",
                    "TDC51D+Z",
                    "TDC51D+Z C5",
                    "CGCC",
                    "DX51D+Z C5",
                    "TDC52D+Z C5",
                    "TDC53D+Z",
                    "TDC54D+Z C5"
                ]
            },
            {
                "name": "彩涂板卷(镀铝锌基板)",
                "value": "TL45",
                "shop_signs": [
                    "TS350GD+AZ",
                    "TDC51D",
                    "TDX51D+AZ",
                    "DC51D",
                    "AZ150"
                ]
            },
            {
                "name": "彩涂尾板卷",
                "value": "TL49",
                "shop_signs": []
            },
            {
                "name": "彩涂板卷(冷轧基板)",
                "value": "TL44",
                "shop_signs": [
                    "TDC51D+Z",
                    "TS280GD+Z",
                    "白灰 TDC51D+Z...",
                    "海蓝 TDC51D+A..."
                ]
            },
            {
                "name": "电镀锌板卷",
                "value": "TL31",
                "shop_signs": [
                    "SECC",
                    "DC01+ZE",
                    "SECCPC5",
                    "SECCNE",
                    "SECCN5",
                    "SECD",
                    "SECDN5",
                    "DC03E+Z",
                    "BLDE+Z",
                    "SECDPC5",
                    "SECE"
                ]
            },
            {
                "name": "电镀锌耐指纹板卷",
                "value": "TL33",
                "shop_signs": [
                    "SECC",
                    "SECCPC5",
                    "SECCNE",
                    "SECCN5",
                    "SECDN5",
                    "EGN5",
                    "SECC-AF5",
                    "BYSE-N5",
                    "EGN5-YS",
                    "DC01E+Z"
                ]
            },
            {
                "name": "电镀锌尾板卷",
                "value": "TL39",
                "shop_signs": [
                    "180B2",
                    "电镀锌尾卷",
                    "BSUFDE+Z 0/...",
                    "DC05E+Z"
                ]
            },
            {
                "name": "镀锡板卷",
                "value": "TL61",
                "shop_signs": [
                    "MR T-4CA",
                    "MR T-5CA",
                    "MIX",
                    "MR DR-9CA",
                    "MR DR-8CA",
                    "TH435",
                    "MR DR-7MCA",
                    "MR T-3CA",
                    "MR-T4CA"
                ]
            },
            {
                "name": "镀铝锌板卷",
                "value": "TL51",
                "shop_signs": [
                    "DC51D+AZ",
                    "CS Type B",
                    "DX51D+AZ",
                    "CS Type B",
                    "DC53D+AZ",
                    "DC54D+AZ",
                    "S350GD+AZ",
                    "FA镀铝锌镁卷"
                ]
            },
            {
                "name": "镀铝锌耐指纹板卷",
                "value": "TL54",
                "shop_signs": [
                    "DC51D+AZ",
                    "DX51D+AZ",
                    "S350GD+AZ",
                    "A792M/S550",
                    "DX51D+AZ-T",
                    "LSGLCCTSEX",
                    "S550GD+AZ",
                    "A792M/CSB"
                ]
            },
            {
                "name": "镀铝锌板卷",
                "value": "TL59",
                "shop_signs": [
                    "DX51D+AZ",
                    "镀铝锌镁尾卷",
                    "FA镀铝锌镁卷",
                    "FA镀铝锌卷",
                    "DX51D+AZ80",
                    "DX53D+AS"
                ]
            },
            {
                "name": "镀铝镁锌板卷",
                "value": "TLG1",
                "shop_signs": [
                    "CR3",
                    "CR4",
                    "HR340LA",
                    "HR380LA",
                    "HR300LA",
                    "DX51D+ZM",
                    "DX54D+ZM",
                    "DC51D+ZM",
                    "S350GD+ZM",
                    "STM3"
                ]
            },
            {
                "name": "冷轧轧硬卷",
                "value": "TL85",
                "shop_signs": [
                    "SPCC",
                    "DC01",
                    "SPCD",
                    "SPCE",
                    "DC04",
                    "DC05",
                    "SPCC-1B",
                    "M250P1",
                    "65Mn",
                    "CDCM-SPCC",
                    "冷轧轧硬卷"
                ]
            },
            {
                "name": "冷成型",
                "value": "TL01",
                "shop_signs": [
                    "MCFC",
                    "SMZG1",
                    "SMMB1（MCFC ...",
                    "RECC",
                    "SMMB1",
                    "SMZG1（MCFC ...",
                    "MCFC"
                ]
            }
        ]
    },
    {
        "code": "RJ",
        "name": "热系板卷",
        "productType": [
            {
                "name": "热轧板卷",
                "value": "TI12",
                "shop_signs": [
                    "SPHC",
                    "IF-1",
                    "SS330",
                    "SPHT1",
                    "SS330-P",
                    "MHS-1",
                    "09CuPCrNi-A",
                    "SS400",
                    "Q460NH",
                    "SS490-P",
                    "Q355NH"
                ]
            },
            {
                "name": "热轧直发卷",
                "value": "TI15",
                "shop_signs": [
                    "SPHC",
                    "SPHC-T",
                    "SPHC R",
                    "IF-1",
                    "IF1 R",
                    "SAE1008",
                    "IF GRADE",
                    "SPHD",
                    "SPHD R",
                    "SPHE",
                    "SPHE R",
                    "SS400"
                ]
            },
            {
                "name": "热轧尾板卷",
                "value": "TI1H",
                "shop_signs": [
                    "SPHC",
                    "DD11",
                    "SPHT1",
                    "SAPH370",
                    "Q235B",
                    "Q345A",
                    "Q550D",
                    "65Mn",
                    "热轧尾卷",
                    "SPA-H",
                    "Q195",
                    "热轧尾板"
                ]
            },
            {
                "name": "热轧热处理板",
                "value": "TI1K",
                "shop_signs": [
                    "LG960QT",
                    "LG900QT",
                    "LG700T",
                    "NM400",
                    "LG700QT",
                    "NM450",
                    "hardox450",
                    "SY680T",
                    "Mn13",
                    "LG1100QT"
                ]
            },
            {
                "name": "热轧开平板",
                "value": "TI1E",
                "shop_signs": [
                    "Q460NH",
                    "Q355NH",
                    "Q235B",
                    "Q450NQR1",
                    "Q345B",
                    "Q460C",
                    "Q550NH",
                    "Q550D",
                    "20CrMo",
                    "42CrMo4",
                    "65Mn"
                ]
            },
            {
                "name": "热轧平整卷",
                "value": "TI13",
                "shop_signs": [
                    "SPHC",
                    "09CuPCrNi-A",
                    "SS400",
                    "Q460NH",
                    "Q235B",
                    "B510L",
                    "B550CL",
                    "SM490A",
                    "Q345B",
                    "BC550",
                    "Q460C"
                ]
            },
            {
                "name": "热轧碳素钢板",
                "value": "TI18",
                "shop_signs": [
                    "42CrMo4",
                    "65Mn",
                    "NM500",
                    "B980LE",
                    "B750LD",
                    "Q355NHB",
                    "45#",
                    "S355J2W",
                    "09CuPCrNiA",
                    "65MN",
                    "NM400"
                ]
            },
            {
                "name": "热轧花纹板卷",
                "value": "TI11",
                "shop_signs": [
                    "HQ235B",
                    "H-Q235B",
                    "AHWB",
                    "H-Q235B(H-Q...",
                    "HQ235"
                ]
            },
            {
                "name": "热轧尾卷",
                "value": "TI19",
                "shop_signs": [
                    "SPHC",
                    "SPHT1",
                    "SS400",
                    "Q355NH",
                    "Q235B",
                    "Q345A",
                    "MR T4",
                    "T5(LC)",
                    "MHR-4",
                    "MR T4.5",
                    "MHR-3"
                ]
            },
            {
                "name": "商品卷",
                "value": "TI14",
                "shop_signs": [
                    "SPHC",
                    "Q235B",
                    "QStE380TM",
                    "MHR-3",
                    "MHR-6",
                    "L485M",
                    "MHR-4",
                    "Q235B-X",
                    "22MnB5，Q500...",
                    "MHR-2"
                ]
            },
            {
                "name": "优质板带",
                "value": "TI1T",
                "shop_signs": [
                    "Q550D",
                    "BWELDY700QL2",
                    "Q690D",
                    "Q500qD",
                    "Q500qDZ15",
                    "Q690E-XGKY"
                ]
            },
            {
                "name": "合金结构钢热轧板卷",
                "value": "TI1U",
                "shop_signs": [
                    "Q550D",
                    "40Cr",
                    "Q355GJB",
                    "510L",
                    "Q420B",
                    "40Mn",
                    "Q355B",
                    "QSTE340TM(Q...",
                    "SK85",
                    "SQ460MCC"
                ]
            },
            {
                "name": "热轧花纹卷",
                "value": "TI17",
                "shop_signs": [
                    "Q235B",
                    "H-Q195P",
                    "H-Q235B",
                    "HQ235B",
                    "Q235B-H",
                    "H-Q235P"
                ]
            },
            {
                "name": "热轧可利用材",
                "value": "TI1I",
                "shop_signs": [
                    "SPHC",
                    "LG700XL",
                    "S350GD+ZM"
                ]
            },
            {
                "name": "汽车大梁用热轧板卷",
                "value": "TI1Y",
                "shop_signs": [
                    "LG750XT",
                    "510L",
                    "610L",
                    "WL510"
                ]
            },
            {
                "name": "热轧带钢",
                "value": "TI1D",
                "shop_signs": [
                    "09CrCuSb",
                    "Q345NS"
                ]
            },
            {
                "name": "热轧横切板",
                "value": "TI1N",
                "shop_signs": [
                    "Q235B",
                    "Q235B-2"
                ]
            },
            {
                "name": "热轧热处理（IJ）",
                "value": "TI1J",
                "shop_signs": [
                    "NM400",
                    "HG785E"
                ]
            },
            {
                "name": "热轧钢带",
                "value": "TI1B",
                "shop_signs": [
                    "SPHC",
                    "Q235B",
                    "B420CL",
                    "B500CL",
                    "LG510L",
                    "65mn",
                    "SPHC-L",
                    "Q195",
                    "S10C"
                ]
            },
            {
                "name": "热轧中间坯",
                "value": "TI1O",
                "shop_signs": [
                    "热轧中间坯"
                ]
            },
            {
                "name": "低合金结构钢热轧板卷",
                "value": "TI1P",
                "shop_signs": [
                    "Q450NQR1",
                    "Q355GNHD",
                    "Q345B",
                    "Q345R",
                    "Q460C",
                    "Q550D",
                    "S355J2W(H)",
                    "Q355D",
                    "S355J2+N",
                    "Q345qDNH"
                ]
            },
            {
                "name": "热轧钢板(合金工具钢)",
                "value": "TI1Z",
                "shop_signs": [
                    "30CrMo",
                    "520JJ",
                    "35CrMo",
                    "BS800E"
                ]
            },
            {
                "name": "酸洗板卷",
                "value": "TL91",
                "shop_signs": [
                    "SPHC",
                    "DD11",
                    "SPHC-P",
                    "SAE 1008",
                    "SP221P",
                    "SP221PQ",
                    "SPHD",
                    "DD12",
                    "SPHE",
                    "DD13",
                    "SAPH370",
                    "SS400"
                ]
            },
            {
                "name": "酸洗带钢",
                "value": "TL9A",
                "shop_signs": [
                    "SPHC",
                    "SPHC-P",
                    "SAPH440",
                    "50#",
                    "65Mn",
                    "MH590FB-P",
                    "QSTE420TM-P",
                    "SPHC:LO"
                ]
            },
            {
                "name": "酸洗尾板卷",
                "value": "TL99",
                "shop_signs": [
                    "SPHC",
                    "SPHE",
                    "St37-2",
                    "QStE500TM",
                    "Q345B",
                    "BTC330R",
                    "酸洗尾卷",
                    "BGMY",
                    "SAPH440参考SA..."
                ]
            },
            {
                "name": "酸洗热轧窄带",
                "value": "TL93",
                "shop_signs": [
                    "Q235B"
                ]
            }
        ]
    },
    {
        "code": "TG",
        "name": "特钢/其他",
        "productType": [
            {
                "name": "热轧棒钢",
                "value": "TB11",
                "shop_signs": [
                    "Q355D",
                    "38MnVS6",
                    "42CrMo",
                    "48MnV",
                    "SAE 1548",
                    "38MnS6",
                    "45CrNiMo1V",
                    "45MnSiVNbS",
                    "S38MnSiV"
                ]
            },
            {
                "name": "球墨铸铁",
                "value": "TA91",
                "shop_signs": []
            },
            {
                "name": "热轧棒(合金工具钢)",
                "value": "SA13",
                "shop_signs": [
                    "16mncr5"
                ]
            },
            {
                "name": "特钢热轧棒",
                "value": "SA1B",
                "shop_signs": []
            }
        ]
    },
    {
        "code": "XC",
        "name": "型材",
        "productType": [
            {
                "name": "H型钢",
                "value": "TX51",
                "shop_signs": [
                    "Q235B",
                    "S355J0",
                    "Q345B",
                    "S355J2",
                    "Q355B",
                    "Q235",
                    "06Cr19Ni10"
                ]
            },
            {
                "name": "槽钢",
                "value": "TX11",
                "shop_signs": [
                    "Q235B",
                    "S355J0",
                    "S235JR",
                    "Q345C",
                    "A36",
                    "Q235A",
                    "Q235",
                    "A992",
                    "S275JR",
                    "Q355B"
                ]
            },
            {
                "name": "叉车用门架槽钢",
                "value": "TX14",
                "shop_signs": [
                    "20MnSiV"
                ]
            },
            {
                "name": "工字钢",
                "value": "TX61",
                "shop_signs": [
                    "Q235B",
                    "Q235"
                ]
            },
            {
                "name": "热轧工字钢",
                "value": "TX62",
                "shop_signs": [
                    "S235JR",
                    "Q345B",
                    "Q355B"
                ]
            },
            {
                "name": "热轧等边角钢",
                "value": "TX22",
                "shop_signs": [
                    "Q235B"
                ]
            },
            {
                "name": "角钢",
                "value": "TX21",
                "shop_signs": [
                    "Q235B",
                    "Q235"
                ]
            },
            {
                "name": "型材正品带出(武钢)",
                "value": "TX8F",
                "shop_signs": [
                    "U71Mn"
                ]
            },
            {
                "name": "履带用型钢",
                "value": "TX85",
                "shop_signs": [
                    "25MnB"
                ]
            }
        ]
    },
    {
        "code": "ZHB",
        "name": "中厚板",
        "productType": [
            {
                "name": "中厚板",
                "value": "TI9D",
                "shop_signs": [
                    "Q235B",
                    "Q345E",
                    "SM570",
                    "Q345C",
                    "Q345B",
                    "Q345R",
                    "Q460C",
                    "Q355B",
                    "Q355D",
                    "Q355NE",
                    "Q355ND"
                ]
            },
            {
                "name": "容器钢板",
                "value": "TI98",
                "shop_signs": [
                    "Q345R",
                    "Q245R",
                    "09MnNiDR",
                    "SA-516Gr.70",
                    "12MnNiVR",
                    "12MnNiVR-SR",
                    "15CrMoR",
                    "A387 Gr.22 ...",
                    "B610CF"
                ]
            },
            {
                "name": "低合金中板",
                "value": "TI9E",
                "shop_signs": [
                    "S355J0",
                    "SM490A",
                    "Q345E",
                    "Q345C",
                    "Q345B",
                    "Q345A",
                    "Q460C",
                    "Q355B",
                    "Q355NE",
                    "Q355NB-Z15",
                    "S420ML"
                ]
            },
            {
                "name": "高强结构板",
                "value": "TI96",
                "shop_signs": [
                    "Q460C",
                    "Q500D",
                    "Q550D",
                    "Q960E",
                    "Q355ND",
                    "Q420C",
                    "Q550C",
                    "Q890D",
                    "Q420B",
                    "Q355NC",
                    "Q390C"
                ]
            },
            {
                "name": "造船钢板",
                "value": "TI92",
                "shop_signs": [
                    "A",
                    "B",
                    "D",
                    "AH32",
                    "AH36",
                    "EH36",
                    "DH36",
                    "DH32",
                    "EH32",
                    "E",
                    "EH47",
                    "AH40",
                    "EH47CAS-Z35"
                ]
            },
            {
                "name": "桥梁板",
                "value": "TI94",
                "shop_signs": [
                    "Q345qD",
                    "Q420qD",
                    "Q370qE",
                    "Q345qC",
                    "Q345qE",
                    "HPS 345WF",
                    "HPS 485WF",
                    "A709-50T-2",
                    "A709-50T-2-..."
                ]
            },
            {
                "name": "普通结构板",
                "value": "TI93",
                "shop_signs": [
                    "SS400",
                    "Q235B",
                    "SM400A",
                    "Q355C",
                    "Q235A",
                    "Q355D",
                    "Q235C",
                    "Q355MB",
                    "Q355C-Z15",
                    "Q390B-Z25"
                ]
            },
            {
                "name": "耐磨板",
                "value": "TI9B",
                "shop_signs": [
                    "NM400",
                    "NM450",
                    "NM600",
                    "NM400A",
                    "B-HARD400B",
                    "B-HARD360B",
                    "NM360",
                    "NM500"
                ]
            },
            {
                "name": "低合金板",
                "value": "TI9U",
                "shop_signs": [
                    "Q345A",
                    "Q355B",
                    "Q355D",
                    "Q355NE",
                    "Q345GJC",
                    "Q345GJCZ15",
                    "Q355NB",
                    "Q355NDZ15"
                ]
            },
            {
                "name": "模具钢板",
                "value": "TI9C",
                "shop_signs": [
                    "42CrMo",
                    "40Cr",
                    "BSM45C",
                    "BSM48C"
                ]
            },
            {
                "name": "工程机械用钢板",
                "value": "TI9A",
                "shop_signs": [
                    "Q460C",
                    "Q550D",
                    "Q460D",
                    "Q890D Z15",
                    "S690Q",
                    "BWELDY960QL4",
                    "Q690D",
                    "Q690E",
                    "Q960E"
                ]
            },
            {
                "name": "管线钢板",
                "value": "TI91",
                "shop_signs": [
                    "L485M",
                    "X70M"
                ]
            },
            {
                "name": "建筑结构板",
                "value": "TI95",
                "shop_signs": [
                    "Q390GJC",
                    "Q345GJC",
                    "Q345GJC-Z25",
                    "Q345GJB",
                    "Q345GJC-Z15",
                    "S890Q",
                    "Q345GJB-Z15",
                    "Q345GJD-Z35"
                ]
            },
            {
                "name": "低温压力容器用钢板",
                "value": "TI9R",
                "shop_signs": []
            },
            {
                "name": "海洋工程用结构钢板",
                "value": "TI9N",
                "shop_signs": []
            },
            {
                "name": "高强度焊接结构钢板",
                "value": "TI9Q",
                "shop_signs": [
                    "Q690CFD"
                ]
            },
            {
                "name": "耐腐蚀板",
                "value": "TI99",
                "shop_signs": [
                    "Q370qENHY-I"
                ]
            }
        ]
    }
]
