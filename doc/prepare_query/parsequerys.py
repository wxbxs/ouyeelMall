# -*- coding: utf-8 -*-
# CreateDate : 2021/12/23 18:38
# Author     : 不肖生
# Github     : https://gitee.com/wxbxs/ouyeelMall
# EditDate   : 
# sourceProj : ouyeelMall
# Description:
import json
import os.path

from ouyeelMall.constant import ProjectPath


class ParseQuery:
    def __init__(self):
        self.codes = {
            'BXG': '不锈钢',
            'GG': '硅钢',
            'GJ': '钢筋',
            'GXC': '钢管',
            'LJ': '冷系板卷',
            'RJ': '热系板卷',
            'TG': '特钢/其他',
            'XC': '型材',
            'ZHB': '中厚板',
        }

    def parse_all(self):
        """
        :return:
        """
        querys = []
        for code, name in self.codes.items():
            filepath = os.path.join(ProjectPath, f"doc/mallitem_ouyeel/IronQuery{code}.json")
            result = self.parse(filepath)
            result['name'] = name
            result['code'] = code
            querys.append(result)

        return querys

    @staticmethod
    def parse(filepath):
        """"""
        with open(filepath, 'r', encoding='utf-8') as fp:
            data = json.loads(fp.read())

        shopSigns = []
        info_1s = data['shopSign']['info']
        for info_1 in info_1s:
            info_2s = info_1['info']
            for info_2 in info_2s:
                info_3s = info_2['info']
                for info_3 in info_3s:
                    shopSigns.append({'name': info_3['name'], 'value': info_3['value']})

        productTypes = []
        type_info_1s = data['productType']['info']
        for type_info_1 in type_info_1s:
            type_info_2s = type_info_1['info']
            for type_info_2 in type_info_2s:
                productTypes.append({'name': type_info_2['name'], 'value': type_info_2['value']})

        # warehouses = []
        # for wh in data['warehouse']['info']:
        #     warehouses.append({'name': wh['name'], "value": wh['value']})

        result = {"productType": productTypes, "shopSigns": shopSigns}
        return result
