# -*- coding: utf-8 -*-
# CreateDate : 2021/12/25 19:53
# Author     : 不肖生
# Github     : https://gitee.com/wxbxs/ouyeelMall
# EditDate   : 
# sourceProj : ouyeelMall
# Description: 先请求一次type页面，得到每一个type下面的牌号，这样可以省去请求无数据的页面
import json
import os.path
import time
from datetime import datetime

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from ouyeelMall.api.seleniumcrawler import SeleniumBrowser
from ouyeelMall.constant import ProjectPath
from doc.prepare_query.parsequerys import ParseQuery


class TypeShopsign:
    def __init__(self):
        self.querys = ParseQuery().parse_all()
        self.url_format = "https://www.ouyeel.com/search-ng/queryResource/indexxhzy?productType={pt}&channel={code}"

        self.result = []

    def get(self):
        for channel in self.querys:

            time.sleep(5)
            c_result = dict()

            c_code = channel['code']
            c_name = channel['name']

            c_result['code'] = c_code
            c_result['name'] = c_name
            c_result['productType'] = []

            pts = channel['productType']
            for pt in pts:
                # 每次链接重新开启浏览器，以免统一浏览器加载次数增多后会失败
                sb = SeleniumBrowser()
                browser = sb.browser
                browser.get("https://www.ouyeel.com")

                pt_name = pt['name']
                pt_value = pt['value']
                url = self.url_format.format(pt=pt_value, code=c_code)
                browser.get(url)
                time.sleep(5)

                try:
                    WebDriverWait(browser, 60).until(
                        EC.presence_of_element_located(
                            (By.CSS_SELECTOR, '#shopSign div[class^="shopsign"]')
                        )
                    )
                except TimeoutException:
                    pass

                shop_sign_elemts = browser.find_elements(
                    by=By.CSS_SELECTOR,
                    value='#shopSign div[class^="shopsign"]>div[class^="flatData"]>span[class^="flatData_item"]'
                )
                shopsigns = [sse.text for sse in shop_sign_elemts if sse.text.strip()]
                pt_result = {
                    "name": pt_name,
                    "value": pt_value,
                    "shop_signs": shopsigns
                }
                c_result['productType'].append(pt_result)
                print(datetime.now(), c_code, pt_value, shopsigns)
                time.sleep(5)

            self.result.append(c_result)

        print(datetime.now(), "done")

    def save(self):
        filepath = os.path.join(ProjectPath, "doc/ouyeel_cps.py")
        with open(filepath, 'w', encoding="utf-8") as fp:
            fp.write(json.dumps(self.result, ensure_ascii=False))


if __name__ == '__main__':
    ts = TypeShopsign()
    ts.get()
    ts.save()
