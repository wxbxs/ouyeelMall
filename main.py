# -*- coding: utf-8 -*-
# CreateDate : 2021/12/15 23:24
# Author     : 不肖生
# Github     :
# EditDate   : 
# sourceProj : wuliu_news
# Description:
#              crawlab 部署调度爬虫可 忽略 此文件

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from scrapy.spiderloader import SpiderLoader

if __name__ == '__main__':

    # 根据项目配置获取 CrawlerProcess 实例
    process = CrawlerProcess(get_project_settings())

    # 以下二选一
    # 1.运行全部爬虫
    # 获取 spiderloader 对象，以进一步获取项目下所有爬虫名称
    # spider_loader = SpiderLoader(get_project_settings())
    # for spidername in spider_loader.list():
    #     process.crawl(spidername)

    # 2.按需添加需要执行的爬虫
    process.crawl('ouyeel')

    # 执行
    process.start()

    # argvs = sys.argv[1:]
    # if not argvs:
    #     argvs = input("输入爬虫名称 -n qyalfx：").split()
    # Param = parse_cmd_params(argvs)
    # cmdline.execute(["scrapy", "crawl", Param.spider])
